FROM python:3.10
WORKDIR /code
RUN adduser appuser
COPY requirements.txt .
RUN pip3 install -r requirements.txt
COPY . .
RUN chown -R appuser /code
RUN chmod +x ./start.sh
USER appuser
CMD ["./start.sh"]
